public class CalenderItem
{
    private int month;
    private int day;
    private String event;

    public CalenderItem(int m, int d, String e)
    {
        month = m;
        day = d;
        event = e;
    }

    public int getMonth()
    {
        return month;
    }

    public int getDay()
    {
        return day;
    }

    public String getEvent()
    {
        return event;
    }
}
