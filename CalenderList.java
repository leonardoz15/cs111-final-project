import java.util.*;
import java.io.*;

public class CalenderList {

    private ArrayList<CalenderItem> calenderItems;
    private static final String CalenderFile = "Calender.txt";
        //Constructor
    public CalenderList() {
        calenderItems = new ArrayList<CalenderItem>();
    }
    public void addCalenderItem(CalenderItem calenderItem) {
        calenderItems.add(calenderItem);
    }

    //Iterator that returns calenderItems seperately
    public Iterator getCalenderItems(){
        return calenderItems.iterator();
    }

    public void readCalenderItems() throws IOException {
        Scanner fileScanner = new Scanner(new File(CalenderFile));
        while(fileScanner.hasNext()) {
            String CalenderLine = fileScanner.nextLine();
            Scanner calenderScanner = new Scanner(CalenderLine);
            calenderScanner.useDelimiter(",");
            String month, day, event;
            month = calenderScanner.next();
            day = calenderScanner.next();
            event = calenderScanner.next();
            int Month = Integer.parseInt(month);
            int Day = Integer.parseInt(day);
            CalenderItem calenderItem = new CalenderItem(Month, Day, event);
            calenderItems.add(calenderItem);
        }
    }
    public void findEventByMonth(int chosenMonth) {
       // ArrayList<CalenderItem> monthList = new ArrayList<CalenderItem>();
        Iterator iterator = calenderItems.iterator();

        while(iterator.hasNext()) {
        CalenderItem calenderItem = (CalenderItem)iterator.next();
        if(calenderItem.getMonth() == chosenMonth){
            //monthList.add(calenderItem);
            System.out.println(calenderItem.getEvent());
        }
    }
       // Iterator monthIterator = monthList.iterator();
        //while(monthIterator.hasNext()){
       //     System.out.println((CalenderItem)(monthIterator.next()).getEvent());
        //}
    }

    public void findEventByMD(int chosenMonth, int chosenDay) {
           // ArrayList<CalenderItem> MDList = new ArrayList<CalenderItem>();
            Iterator iterator = calenderItems.iterator();

        while(iterator.hasNext()) {
            CalenderItem calenderItem = (CalenderItem)iterator.next();
            if(calenderItem.getMonth() == chosenMonth && calenderItem.getDay() == chosenDay){
                //MDList.add(calenderItem);
                System.out.println(calenderItem.getEvent());
            }
        }
        //Iterator MDiterator = MDList.iterator();
       // while(MDiterator.hasNext()){
       //     System.out.println(MDiterator.next().getEvent());
       // }
    }

    public void findEvent(String chosenEvent) {
       // ArrayList<CalenderItem> eventList = new ArrrayList<CalenderItem>();
        Iterator iterator = calenderItems.iterator();
        while(iterator.hasNext()) {
            CalenderItem calenderItem = (CalenderItem)iterator.next();
            if(calenderItem.getEvent().contains(chosenEvent)) {
                //eventList.add(calenderItem);
                System.out.print(calenderItem.getMonth()+"/"+calenderItem.getDay()+"\n");
            }
        }
        //return eventList.iterator();
    }

    public void list() {
        //StringBuffer buffer = new StringBuffer();
        Iterator iterator = calenderItems.iterator();
        while(iterator.hasNext()) {
            //buffer.append(
            CalenderItem calenderItem = (CalenderItem)iterator.next();
            //if(iterator.hasNext()) {
           //     buffer.append("\n");
           // }
            System.out.println(calenderItem.getEvent());
        }
        //return buffer.toString();
    }

    //Maybe put in the countdown method here
}
